map = PragueHeightMap("prague_height_map.jpg");
disp("Map loaded.")

lat = [
    50.0732656;
    50.0831319;
    50.0973703;
    50.0690117;
];

lon = [
    14.3648147;
    14.4014031;
    14.4171825;
    14.4143447;
];

% vysky dle map cz
h_def = [
    304;
    224;
    227;
    188;
];


lat = deg2rad(lat);
lon = deg2rad(lon);
H = map.get_height_wgs84(lat, lon);
disp("Vyska databaze, odhad mapy cz, rozdil");
disp([H, h_def, H-h_def]);

