classdef PragueHeightMap
    %PRAGUEHEIGHTMAP Wrapper around height map of Prague
    
    properties (Access = protected)
        offset = 166.7570
        scale = 340.5780
        map
    end
    
    methods
        function obj = PragueHeightMap(filename)
            %PRAGUEHEIGHTMAP Construct an instance of this class

            obj.map = imread(filename);
        end
        
        function H = get_height_sjtsk(obj,Y, X)
            %GET_HEIGHT_SJTSK Get geight from file using S-JTSK coordinates
            %   both arguments must be vectors

            assert(isvector(Y));
            assert(isvector(X));
            assert(length(Y) == length(X));

            col = round(757499.5000000000 - Y);
            row = round(X-1032000.5000000000);

            H = zeros(size(Y));

            nan_i = row < 1 | row > size(obj.map,1) | col < 1 | col > size(obj.map,2);
            H(nan_i) = NaN;
            H(~nan_i) = obj.map(sub2ind(size(obj.map), row(~nan_i), col(~nan_i)));
            H(H == 0) = NaN;
            H = (H-1) * obj.scale/(2^16-1) + obj.offset;
        end

        function alt = get_height_wgs84(obj, lat, lon)
            %GET_HEIGHT_WGS84 Get geight from file using S-JTSK coordinates
            %   both arguments must be vectors

            assert(isvector(lat));
            assert(isvector(lon));
            assert(length(lat) == length(lon))

            default_alt = 275*ones(size(lat));
           

            [Y, X, ~] = wgs84_to_sjtsk(lat, lon, default_alt);
            alt = obj.get_height_sjtsk(Y,X);
        end

        function show_map(obj)
            imshow(obj.map);
        end

        function map = get_map(obj)
            map = (double(obj.map) - 1)*obj.scale/(2^16-1) + obj.offset;
            map(obj.map == 0) = NaN;
        end
    end
end

